﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AppPostulaciónFeriasLoja.Startup))]
namespace AppPostulaciónFeriasLoja
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
